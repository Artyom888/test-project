<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'login'], function () {
    Route::get('/', ['as' => 'show.login', 'uses' => 'Auth\LoginController@show']);
    Route::post('/', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
});

Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::group(['prefix' => 'register'], function () {
    Route::get('/', ['as' => 'show.register', 'uses' => 'Auth\RegisterController@show']);
    Route::post('/', ['as' => 'register', 'uses' => 'Auth\RegisterController@register']);
});
Route::group(['prefix' => 'password'], function () {
    Route::get('/reset', ['as' => 'show.password.reset', 'uses' => 'Auth\PasswordResetController@show']);
    Route::post('/reset', ['as' => 'send.password.reset', 'uses' => 'Auth\PasswordResetController@sendResetEmailLink']);
    Route::get('/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\PasswordResetController@passwordReset']);
    Route::post('/change', ['as' => 'change.password', 'uses' => 'Auth\PasswordResetController@changePassword']);

});

Route::group(['prefix' => '/account/activate'], function () {
    Route::get('/', ['as'=> 'show.account.activate','uses'=>'Auth\AccountActivateController@show']);
    Route::get('/{token}', ['as'=> 'account.activate','uses'=>'Auth\AccountActivateController@activate']);
    Route::get('/resend/email', ['as'=> 'show.resend.email','uses'=>'Auth\AccountActivateController@showResend']);
    Route::post('/resend/email', ['as'=> 'resend.email','uses'=>'Auth\AccountActivateController@resendActivateEmail']);
});

Route::get('/', ['as'=> 'show.home','uses'=>'HomeController@index']);

Route::group(['middleware' => 'myAuth'], function () {
    Route::get('/posts', ['as' => 'show.allPosts', 'uses' => 'PostsController@showAll']);
    Route::get('/post/create', ['as' => 'show.createForm', 'uses' => 'PostsController@showForm']);
    Route::post('/post/create', ['as' => 'post.create', 'uses' => 'PostsController@create']);
    Route::get('/post/{id}', ['as' => 'show.post', 'uses' => 'PostsController@showPost']);
});