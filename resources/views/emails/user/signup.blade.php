<p>Hi {{ $name }}</p>
<p>Thank you for signing up.</p>
<p>Here is your login credentials:</p>
<p><strong>Email:</strong> {{ $email }}</p>
<p><strong>Password:</strong>{{ $password }}</p>

<p>Follow to this link for activate your account <a href="{{ url('/account/activate',[$token])}}">Click here</a></p>