@extends('layouts.index')
@section('content')
    <div class="d-flex flex-wrap bd-highlight">
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                <a class="close font-weight-light" data-dismiss="alert" href="#">×</a>
                {{ $error }}
            </div>
        @endforeach
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

        @foreach($posts as $post)
            <div class="row">
                <div class="p-2 bd-highlight">
                    <div class="card post" style="width: 18rem;">
                        <div class="card-post-image">
                            <img class="card-img-top img-responsive menu-thumbnails" src="<?php echo asset('storage/post/images/'.$post->image);?>" alt="Card image cap">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $post->title }}</h5>
                            <p class="card-text">
                                {{ $post->description }}
                            </p>
                            <a href="{{ route('show.post', $post->id)  }}" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="col-lg-12">
        {{ $posts->links() }}
    </div>
@endsection