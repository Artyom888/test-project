@extends('layouts.index')
@section('content')
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
            <a class="close font-weight-light" data-dismiss="alert" href="#">×</a>
            {{ $error }}
        </div>
    @endforeach
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    <div class="col-md-8">
        <form action="{{  route('post.create') }}" method='post' enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="postTitle">Post Title</label>
                <input type="text" class="form-control" id="postTitle" name="title" value="{{ old('title') }}">
            </div>

            <div class="custom-file">
                <input type="file" class="custom-file-input" name="postImg" id="customFile">
                <label class="custom-file-label" data-label-name="Choose file" for="customFile">Choose file</label>
            </div>

            <div class="form-group">
                <textarea class="form-control" name="postDesc" id="post-desc" rows="3"  placeholder="Type post description">
                    {{ old('postDesc') }}
                </textarea>
            </div>

            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>

@endsection