@extends('layouts.index')
@section('content')

    <div class="col-md-8 show-post">
        <h5 class="post-title">{{$post->title}}</h5>
        <div class="col-md-12 post-image">
            <img class=img-responsive src="{{ url('storage/post/images//'.$post->image) }}" alt="image">
            <hr>
        </div>

        <div class="col-md-12 post-desc">
            {{ $post->description }}
        </div>
    </div>

@endsection