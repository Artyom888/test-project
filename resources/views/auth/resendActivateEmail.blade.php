@extends('layouts.index')
@section('content')

    <div class="row">
        <div class="col-md-7 mx-auto">
            <div class="card card-body">
                <h3 class="text-left mb-4">Resennd account activation link</h3>
                @if (session('sendEmail'))
                    <div class="alert alert-success">
                        {{ session('sendEmail') }}
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        <a class="close font-weight-light" data-dismiss="alert" href="#">×</a>
                        {{ $error }}
                    </div>
                @endforeach
                <fieldset>
                    <form class="form-horizontal" method="POST" action="{{ route('resend.email') }}">
                        {{ csrf_field() }}

                        <div class="col-md-6 ">
                            <label for="sendEmail">E-mail Address</label>
                            <input class="form-control form-control-sm" name="email" id="sendEmail" type="text" aria-describedby="emailHelpBlock">
                            <small id="emailHelpBlock" class="form-text text-muted">Enter the email to which we will send the link to active account</small>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send activation Link
                                </button>
                            </div>
                        </div>
                    </form>
                </fieldset>
            </div>
        </div>
    </div>

@endsection