@extends('layouts.index')
@section('content')
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body">
                <h3 class="text-center mb-4">Change Password</h3>
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        <a class="close font-weight-light" data-dismiss="alert" href="#">×</a>
                        {{ $error }}
                    </div>
                @endforeach
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                <fieldset>
                    <form method="post" action="{{ route('change.password') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <input class="form-control input-lg" placeholder="New Password" name="password" type="password">
                            <small id="passwordHelpBlock" class="form-text text-muted">The password must be at least 4 characters and must contain numbers and letters</small>
                        </div>
                        <div class="form-group">
                            <input class="form-control input-lg" placeholder="Confirm New Password" name="password_confirmation" type="password">
                        </div>
                        <input name="id" value="{{$user->id}}" type="hidden">
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Change</button>
                    </form>
                </fieldset>
            </div>
        </div>
    </div>
@endsection