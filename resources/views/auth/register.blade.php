@extends('layouts.index')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card card-body">
                    <h3 class="text-center mb-4">Sign-up</h3>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            <a class="close font-weight-light" data-dismiss="alert" href="#">×</a>
                            {{ $error }}
                        </div>
                    @endforeach

                    <fieldset>
                        <form method="post" action="{{ route('register') }}">
                            {{ csrf_field()  }}
                            <div class="form-group">
                                <input class="form-control input-lg" placeholder="Username" name="name" type="text" value="{{old('name')}}">
                            </div>
                            <div class="form-group">
                                <input class="form-control input-lg" placeholder="E-mail Address" name="email" type="text" value="{{old('email')}}">
                            </div>
                            <div class="form-group">
                                <input class="form-control input-lg" placeholder="Password" name="password"
                                       type="password" aria-describedby="passwordHelpBlock">
                                <small id="passwordHelpBlock" class="form-text text-muted">The password must be at least 4 characters and must contain numbers and letters</small>
                            </div>
                            <div class="form-group">
                                <input class="form-control input-lg" placeholder="Confirm Password" name="password_confirmation"
                                       type="password">
                            </div>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign Me Up</button>
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
@endsection