@extends('layouts.index')
@section('content')
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body">
                <h3 class="text-center mb-4">Sign-in</h3>
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        <a class="close font-weight-light" data-dismiss="alert" href="#">×</a>
                        {{ $error }}
                        @if($errors->has('verifyExc'))
                            <button class="btn btn-warning resend-activate-email"><a href="{{ route('show.resend.email')}}">To send again click here</a></button>
                        @endIf
                    </div>
                @endforeach
                @if (session('accountActivated'))
                    <div class="alert alert-success" role="alert">
                        {{ session('accountActivated') }}
                    </div>
                @endif
                <fieldset>
                    <form method="post" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input class="form-control input-lg"
                                   value="{{ Cookie::get('remember_token') !== null ? Crypt::decrypt(Cookie::get('remember_email')) : old('email') }}"
                                   placeholder="E-mail Address" name="email" type="text">
                        </div>

                        <div class="form-group">
                            <input class="form-control input-lg" placeholder="Password" name="password"
                                   type="password">
                        </div>

                        <div class="d-flex bd-highlight">
                            <div class="p-2 flex-grow-1 bd-highlight">
                                <div class="remember-checkbox">
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="remember">Remember Me</label>
                                </div>
                            </div>
                            <div class="p-2 bd-highlight">
                                <div class="forgot-pass">
                                    <label for="forgot-pass"></label>
                                    <a href="{{ route('show.password.reset') }}" id="forgot-pass">Forgot Password?</a>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
                    </form>
                </fieldset>
            </div>
        </div>
    </div>
@endsection