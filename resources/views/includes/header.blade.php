<nav class="navbar navbar-default navbar-expand-lg navbar-light">
    <div class="navbar-header d-flex col-2">
        <a class="navbar-brand" href="">Test<b>Task</b></a>
    </div>

    <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
        <ul class="nav navbar-nav">
            <li class="nav-item"><a href="{{ route('show.home') }}" class="nav-link">Home</a></li>
            <li class="nav-item"><a href="{{ route('show.allPosts') }}" class="nav-link">Posts</a></li>
            <li class="nav-item"><a href="{{ route('show.createForm') }}" class="nav-link">Create Post</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right ml-auto">
            @if (! MyAuth::check())
                <li class="nav-item"><a href="{{ route('show.login')  }}" class="nav-link">Login</a></li>
                <li class="nav-item "><a href="{{ route('show.register') }}" class="nav-link">Sign Up</a></li>
            @else
                <li class="nav-item"><a href="{{ route('logout')  }}" class="nav-link">Logout</a></li>
            @endif
        </ul>
    </div>
</nav>

