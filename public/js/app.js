$(function() {

    $('.custom-file-input').on('change',function() {
        let label = $('.custom-file-label');
        let labelName = $('.custom-file-label').data('label-name');

        if($(this).val()) {
            label.html($(this).val());
        }else{
            label.html(labelName);
        }
    });
});