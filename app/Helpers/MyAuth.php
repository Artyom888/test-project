<?php

namespace App\Helpers;

use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Cookie;

class MyAuth {

	/**
     * @return object|null
	 */
    public function user()
    {
        if (Session::has('email')) {
            $email = Session::get('email');
            $pass = Session::get('password');
            $user = User::where('email', $email)->where('password', $pass)->first();
            return !is_null($pass) ? $user : null;
        } else {
            return null;
        }
    }

    public function check()
    {
        return ! is_null($this->user());
    }

	/**
	 * @param object $user
	 * @param bool $remember
     * @return bool
	 */
	public function login($user , $remember = false) {

        if($user->activated) {
            Session::put(['email' => $user->email, 'password' => $user->password]);
            if ($remember) {
                $this->setRememberToken($user);
            }else{
				Cookie::queue(Cookie::forget('remember_token'));
				Cookie::queue(Cookie::forget('remember_email'));
				$user->remember_token = null;
				$user->save();
			}
            return true;
        }
        return false;
	}

    /**
     * @return bool
     */
    public function logout()
    {
        Session::forget('email');
        if (!Session::has('email')) {
            return true;
        } else {
            return false;
        }
    }

	/**
	 * @param array $credentials
	 * @param bool $remember
     * @return boolean|array
	 */
	public function attempt(array $credentials = [], $remember = false) {

		$user = User::where('email',$credentials['email'])->first();
		if ($this->hasValidCredentials($user, $credentials)) {
            if($this->login($user, $remember)) {
                return true;
            }
            return ['status'=> 'inactive'];
        }
        return false;
	}

	/**
	 * Determine if the user matches the credentials.
	 *
	 * @param  mixed  $user
	 * @param  array  $credentials
	 * @return bool
	 */
	protected function hasValidCredentials($user, $credentials)
	{
		return ! is_null($user) && Hash::check( $credentials['password'], $user->password);
	}


    protected function setRememberToken($user)
    {
        if (! Cookie::get('remember_token')) {
            $token = str_random(60);
            $user->remember_token = $token;
            $user->save();
			Cookie::queue(Cookie::make('remember_token', str_random(60), 1440));
			Cookie::queue(Cookie::make('remember_email', Crypt::encrypt($user->email), 1440));
		}
    }
}