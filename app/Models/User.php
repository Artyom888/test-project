<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','activated',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


	public function userVerify()
	{
        return $this->hasOne('App\Models\UserVerify');
	}


	public function passwordReset()
	{
		return $this->hasOne('App\Models\PasswordReset');
	}

    /**
     * Relationship with `posts` table.
     *
     * @return HasMany
     */
    public function post()
    {
        return $this->HasMany('App\Models\Posts');
    }
}
