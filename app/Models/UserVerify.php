<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVerify extends Model
{
	protected $table = 'user_verifies';
	protected $fillable = ['user_id','token'];
	public $timestamps = false;

	public function user()
	{
		return $this->BelongsTo('App\Models\User');
	}
}
