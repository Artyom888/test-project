<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Posts extends Model
{
   protected $fillable = ['title','description','image'];


    /**
     * Relationship with `user` table.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }
}
