<?php

namespace App\Traits;

use Illuminate\Support\Facades\Validator;

trait ValidateData {

	public function registerValidate ($data) {
		$rules = [
			'name' => 'required|min:4|max:255',
			'email' => 'required|email|max:255|unique:users',
            'password_confirmation' => 'min:4|required_with:password|same:password',
            'password' => 'required|string|min:4|max:32|regex:/^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{4,}$/',
		];
		$error_messages = [];
		return Validator::make($data, $rules,$error_messages);
	}

	public function loginValidate ($data) {
		$rules = [
			'email' => 'required|email|max:255',
            'password' => 'required|string|min:4|max:32|regex:/^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{4,}$/',
		];
		$error_messages = [];
		return Validator::make($data, $rules,$error_messages);
	}


	public function postCreateValidate ($data) {
		$rules = [
			'title' => 'required|string|min:3|max:255',
            'postImg' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:8000',
            'postDesc' => 'required',
		];

		$error_messages = [
		    'required' => 'Please fill all fields '
        ];
		return Validator::make($data, $rules,$error_messages);
	}
}