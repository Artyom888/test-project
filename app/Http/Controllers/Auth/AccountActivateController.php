<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\UserVerify;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Events\SendEmailUserRegisterEvent;

class AccountActivateController extends Controller {
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show() {
		return view('auth.accountActivate');
	}

	public function showResend() {
		return view('auth.resendActivateEmail');
	}

	public function resendActivateEmail(Request $request) {
		try {
			$validator = Validator::make($request->all(), ['email' => 'required|email|max:255']);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator);
			}

			$user = User::where('email', $request->input('email'))->first();
			if (!$user) {
				return false;
			}

			$user->userVerify()->updateOrCreate(['user_id' => $user->id], [
				'token' => str_random(60)
			]);

			event(new SendEmailUserRegisterEvent($user));
			return redirect()->route('show.account.activate');
		} catch (\Exception $e) {
			return redirect()->back()->withErrors(['regExc' => 'Something goes wrong, please try again'])->withInput();
		}
	}

	/**
	 * @param $token
	 * @return bool|\Illuminate\Http\RedirectResponse|void
	 */
	public function activate($token) {
		$userVerify = UserVerify::where('token', $token)->first();
		if ($userVerify->user->activated) {
			return;
		}
		if ($userVerify->user) {
			$userVerify->user->update(['activated' => true]);
			return redirect('/login')->with('accountActivated', 'Your account already activated !');
		}
	}
}
