<?php

namespace App\Http\Controllers\Auth;

use App\Models\PasswordReset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\SendResetPassEmailLinkEvent;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class PasswordResetController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show () {

        return view('auth.passReset');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetEmailLink(Request $request)
    {
        $rules = [
            'email' => 'required|email|max:255',

        ];
        $error_messages = [];

        $validator = Validator::make($request->all(), $rules, $error_messages);
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $email = $request->input('email');
        $user = User::where('email', $email)->first();

        if ($user) {
			$user->passwordReset()->updateOrCreate(['user_id' => $user->id], [
				'token' => str_random(60)
			]);

            event(new SendResetPassEmailLinkEvent($user));
            return redirect()->back()->with('sendEmail', 'We send message in your email, please check');
        }
        return redirect()->back()->withErrors(['error' => 'There is no such user, check again']);

    }

    public function passwordReset ($token)
    {
        $pass = PasswordReset::where('token',$token)->first();
        if(! $pass->user) {
            return ;
        }
        return view('auth.changePass')->with('user',$pass->user);
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request)
    {
        try{
            $rules = [
                'password_confirmation' => 'min:4|required_with:password|same:password',
                'password' => 'required|string|min:4|confirmed|regex:/^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{4,}$/',
            ];

            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }
            $user = User::find($request->input('id'));

            if(!$user) {
                return false;
            }

            if(Hash::check( $request->input('password'), $user->password)) {
                return redirect()->back()->withErrors(['error'=>'This password have already been try another']);
            }

            $user->update(['password' => bcrypt($request->input('password'))]);

            return redirect()->back()->with('success','Your password successfuly changed');

        }catch(\Exception $e) {
            Log::error($e->getFile() .'--' .$e->getLine().'--'.$e->getMessage());
            return redirect()->back()->withErrors(['error' =>'Something goes wrong try again']);
        }

    }

}
