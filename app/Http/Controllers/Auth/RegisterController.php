<?php

namespace App\Http\Controllers\Auth;

use App\Facades\MyAuth;
use App\Models\User;
use App\Events\SendEmailUserRegisterEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ValidateData;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
	use ValidateData;

    public function show () {

        if(MyAuth::check()) {
            return abort(404);
        }
    	return view('auth.register');
	}


    public function register (Request $request)
	{
		try{
			$validator = $this->registerValidate($request->all());
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			$params = [
				'name'=> $request->input('name'),
				'email'=> $request->input('email'),
				'password'=> bcrypt($request->input('password')),
			];

			$user = User::create($params);

			if(!$user) {
				return false;
			}
			$user->userVerify()->create([
				'token' => str_random(60)
			]);

			event(new SendEmailUserRegisterEvent($user));
			return redirect()->route('show.account.activate');
		}catch(\Exception $e) {
			return redirect()->back()->withErrors(['regExc'=> 'Something goes wrong, please try again'])->withInput();
        }
	}


}
