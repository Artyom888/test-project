<?php

namespace App\Http\Controllers\Auth;

use App\Facades\MyAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ValidateData;
use Cookie;

class LoginController extends Controller {

	use ValidateData;

    public function show() {

        if(MyAuth::check()) {
            return abort(404);
        }

        return view('auth.login');
	}

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request) {

		$validator = $this->loginValidate($request->all());
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
		}

        $checkCredentials = MyAuth::attempt($request->only('email', 'password'), $request->filled('remember'));

        if($checkCredentials) {
            if( $checkCredentials['status'] === 'inactive' ) {
                return redirect()->back()->withErrors(['verifyExc' =>'Your account does\'nt active, please activate it!']);
            }else{
                return redirect()->intended('/');
            }
        }
        return redirect()->back()->withErrors(['loginError' => ['Email or password are wrong please try again'] ]);
	}


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout () {
        if(MyAuth::logout()) {
            return redirect('/login');
        }
    }
}
