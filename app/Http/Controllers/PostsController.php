<?php

namespace App\Http\Controllers;

use App\Facades\MyAuth;
use App\Models\Posts;
use App\Traits\ValidateData;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{

    use ValidateData;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAll()
    {
        $posts = MyAuth::user()->post()->paginate(8);

        return view('pages.posts', ['posts' => $posts]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showForm()
    {
        return view('pages.partials.postForm');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        try{
            $validator = $this->postCreateValidate($request->all());

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }

            $file = $request->file('postImg');
            $currentTimestamp = Carbon::now()->timestamp;
            $imgName = rand() . $currentTimestamp;
            $extension = $file->getClientOriginalExtension();
            $thumbImg = Image::make($file->getRealPath())->resize(130, 130)->stream();
            $saveThumb = Storage::put('post/thumbs' . '/' . $imgName . '.' . $extension, $thumbImg);
			$saveImage = Storage::put('post/images' . '/' . $imgName . '.' . $extension, Image::make($file->getRealPath())->stream());
            $image = $imgName . '.' . $extension;

            if($saveThumb && $saveImage) {
				$post = MyAuth::user()->post()->create([
					'title' => $request->input('title'),
					'image' => $image,
					'description' => $request->input('postDesc'),
				]);
			}

            if($post) {
               return redirect()->back()->with('success','Your post successfully created');
            }
        }catch(\Exception $e) {
			Log::error('An error has occurred in '.$e->getFile().'in line'.$e->getLine().'er-'.$e->getMessage());
			return redirect()->back()->withErrors(['error'=> 'Something goes wrong, please try again'])->withInput();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public  function showPost($id) {

        $post = Posts::find($id);

        if(!$post) {
            return redirect()->back();
        }
        return view('pages.partials.showPost', ['post' => $post]);
    }
}
