<?php

namespace App\Http\Middleware;

use Closure;
use \App\Facades\MyAuth;

class MyAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(MyAuth::check()) {
            return $next($request);
        }

        return redirect('/login');

    }
}
