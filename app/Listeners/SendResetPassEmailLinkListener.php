<?php

namespace App\Listeners;

use App\Events\SendResetPassEmailLinkEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendResetPassEmailLinkListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendResetPassEmailLinkEvent  $event
     * @return void
     */
    public function handle(SendResetPassEmailLinkEvent $event)
    {
        $email = $event->user->email;
        $data = [
            'name' => $event->user->name,
            'token' => $event->user->passwordReset->token,
        ];

        \Mail::send('emails.user.passReset', $data, function ($message) use ($email) {
            $message->to($email)->subject('Password Reset');
        });;
    }
}
