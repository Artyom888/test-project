<?php

namespace App\Listeners;

use App\Events\SendEmailUserRegisterEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailUserRegisterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmailUserRegisterEvent  $event
     * @return void
     */
    public function handle(SendEmailUserRegisterEvent $event)
    {
        $email = $event->user->email;

        $params = [
        	'name' => $event->user->email,
        	'email' => $email,
        	'password' => $event->user->password,
        	'token' => $event->user->userVerify->token,
		];

		\Mail::send('emails.user.signup', $params, function ($message) use ($email) {
			$message->to($email)->subject('Activate Your Account');
		});
    }
}
