<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class MyAuth extends Facade{

	protected static function getFacadeAccessor() { return 'myAuth'; }

}